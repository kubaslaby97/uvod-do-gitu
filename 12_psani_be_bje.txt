„Nějaké bábě se zaběhl pes,“ běsnil správce Hříběcí boudy v Krkonoších. 
Měl jsem objednaný oběd, ale dlouho mi ho nepřinášeli, tak jsem šel také obětavě pomoci s hledáním. 
Vyběhli jsme z chaty, nasadili na obě nohy běžky a zběsile se vydali bělavým sněhem po psích stopách. 
Asi pětkrát jsme objeli chatu, ale pes nikde. 
Jen u služebního vchodu byl ze závěje slabě slyšet psí štěkot. 
Vzali jsme lopaty a hrábě (ty nebyly moc užitečné) a začali hrabat stále hlouběji. 
Po nějaké době se objevily dvě zbědované hlavy, psí a lidská. 
Pod objemnou kupou sněhu, která se sesunula ze střechy, zapadl kuchař a pes ho zachránil. 
Tak proto jsem na oběd čekal tak dlouho!
