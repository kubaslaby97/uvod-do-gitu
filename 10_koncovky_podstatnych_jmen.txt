Mezi poli se klikatila cesta posetá klasy. 
Z dáli bylo vidět, jak se po ní blíží Jindra se svými dvěma kamarádkami. 
Obě, i když ověšené falešnými drahokamy, statečně poháněly pedály svých bicyklů. 
Obilí se vlnilo a na obloze bylo vidět orly. 
„Už abychom byli v cíli,“ povídá Jindra. 
Právě se dostali do stínu mezi stromy. 
Rostly tam parádní hřiby. 
Tak nechali kola koly a vrhli se na ně. 
Pak pokračovali podle plánované trasy do blízké vsi. 
Na sinici se rozjeli tak rychle, až jim vlály vlasy. 
Po louce vedle silnice se najednou rozběhly krávy. 
Jindra se lekl a už byl na zemi. 
Zlomil si ruku. 
Před přáteli pak vykládal, že mu do cesty vběhlo koťátko a že upadl schválně, aby jej zachránil.